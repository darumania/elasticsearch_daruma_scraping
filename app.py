from flask import Flask, render_template, request
from main import search

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/search_product', methods=['GET'])
def search_product():
    if request.method == 'GET':
        keyword = request.args.get('keyword')
        products = search(index_name='daruma', keyword=keyword)
        return render_template('index.html', products=products, keyword=keyword)

if __name__ == '__main__':
    app.run(debug=True)