import requests
import json
import csv
import sys
# requests.put('http://localhost:9200/company')
# response = requests.get('http://localhost:9200/company')
# print(json.dumps(response.json(), indent=4))

# response = requests.post('http://localhost:9200/company/1', json={
#     "name" : "Farhan Muhammad",
#     "occupation" : "Software Engineer"
# })
# print(json.dumps(response.json(), indent=4))


fileds = 'id, name, brand, price_before_discount, price, sku, supplier_code, description, item_url, store, last_update, daruma_code, rating, store_url, image_url, link'

def read_csv_item(csvfile):
    result = []
    with open(csvfile, newline='', encoding='utf-8') as f:
        readers = csv.reader(f)
        for row in readers:
            id = row[0]
            name = row[1]
            brand = row[2]
            price_before_discount = row[3]
            price = row[4]
            sku = row[5]
            supplier_code = row[6]
            description = row[7]
            item_url = row[8]
            store = row[9]
            last_update = row[10]
            daruma_code = row[11]
            rating = row[12]
            store_url = row[13]
            image_url = row[14]
            link_id = row[15]
            result.append(dict(
                id=id,
                name=name,
                brand=brand,
                price_before_discount=price_before_discount,
                price=price,
                sku=sku,
                supplier_code=supplier_code,
                description=description,
                item_url=item_url,
                store=store,
                last_update=last_update,
                daruma_code=daruma_code,
                rating=rating,
                store_url=store_url,
                image_url=image_url,
                link_id=link_id
            ))
    return result[1:]

# print(read_csv_item(csvfile='price_table.csv')[:2])

latest = 'Wipro Zinc Cutter (Gunting Seng British Type) WS 2010 Green-Silver 10inch 1pc was inserted (8599/63394) 13.56%'

lis = [1,2,3,4,5,6,7,8]
print(lis[2:])
# print(sys.getdefaultencoding())