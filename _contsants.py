from elasticsearch import Elasticsearch
import elasticsearch
import logging

HOST = '46.101.77.153'
# HOST = '172.22.0.3'
PORT =  9200

es_object = Elasticsearch([{
            'host': HOST,
            'port': PORT
        }])


def connect():

    try:
        _es = None
        _es = Elasticsearch([{
            'host': HOST,
            'port': PORT
        }])
        if _es.ping():
            print('connected ..')
            return _es
        else:
            print('Aww it could not connected!')
    except (Exception, elasticsearch.ElasticsearchException) as e:
        print('Error: ', e)

def get_products():

    result = list()
    for i in range(len(products)):
        name = products[i].get('name')
        suggest = name
        catalog_code = products[i].get('catalog_code')
        images = products[i].get('images')[0].get('original_url')
        brand_name = products[i].get('brand_name')
        description = products[i].get('description')
        price_before_discount = products[i].get('price_before_discount')
        price = products[i].get('price', None)
        d = dict(name=name,
                 suggest=suggest,
                 catalog_code=catalog_code,
                 images=images,
                 brand_name=brand_name,
                 description=description,
                 price_before_discount=price_before_discount,
                 price=price)
        result.append(d)
    return result


if __name__ == '__main__':
    # logging.basicConfig(level=logging.ERROR)
    # connect()
    print(get_products())
    print(len(products))