import requests
import json
import _contsants
import csv
import sys

def create_index(index_name):

    analyzer = "word_join_analyzer"

    created = False
    es_object = _contsants.es_object
    # index settings
    setting = {
        "settings": {
            "number_of_shards": 1,
            "number_of_replicas": 0,
            "analysis" : {
                "filter" : {
                    "word_joiner" : {
                        "type" : "shingle",
                        "output" : "true",
                        "min_shingle_size" : 2,
                        "max_shingle_size" : 20,
                        "token_separator" : ""
                    }
                },
                "analyzer" : {
                    "word_join_analyzer" : {
                        "type" : "custom",
                        "tokenizer" : "standard",
                        "filter" : [
                            "lowercase",
                            "word_joiner"
                        ]
                    }
                }
            }
        },
        "mappings": {
            "price": {
                "dynamic": "strict",
                "properties": {
                    "id": {
                        "type": "integer"
                    },
                    "name": {
                        "type": "text",
                        "analyzer": analyzer,
                        "fielddata": True
                    },
                    "brand": {
                        "type": "text",
                        "analyzer": analyzer,
                        "fielddata": True
                    },
                    "price_before_discount": {
                        "type": "integer"
                    },
                    "price": {
                        "type": "integer"
                    },
                    "sku": {
                        "type": "text",
                        "fielddata": True
                    },
                    "supplier_code": {
                        "type": "text",
                        "analyzer": analyzer
                    },
                    "description": {
                        "type": "text",
                        "analyzer": analyzer,
                        "fielddata": True
                    },
                    "item_url": {
                        "type": "text"
                    },
                    "store": {
                        "type": "text",
                        "analyzer": analyzer,
                        "fielddata": True
                    },
                    "last_update": {
                        "type": "text"
                    },
                    "daruma_code": {
                        "type": "text",
                        "analyzer": analyzer
                    },
                    "rating": {
                        "type": "text"
                    },
                    "store_url": {
                        "type": "text",
                        "analyzer": analyzer
                    },
                    "image_url": {
                        "type": "text"
                    },
                    "link_id": {
                        "type": "text",
                        "analyzer": analyzer
                    },
                    "sales": {
                        "type": "integer"
                    },
                    "views": {
                        "type": "integer"
                    },
                    "reviews": {
                        "type": "integer"
                    },
                    "total_sales": {
                        "type": "integer"
                    },

                }
            }
        }
    }

    try:
        if not es_object.indices.exists(index_name):
            es_object.indices.create(index=index_name, ignore=404, body=setting)
            print('index was created..')
            created = True
        else:
            print('index not created :(')
    except Exception as e:
        print('Error: ', e)
    finally:
        return created

def put_mappings():

    es_object = _contsants.es_object
    mappings = {
                    "properties": {
                        "name": {
                            "type": "text",
                            "fielddata": True,
                            "analyzer": "word_join_analyzer"
                        },
                        "brand": {
                            "type": "text",
                            "fielddata": True,
                            "analyzer": "word_join_analyzer"
                        },
                        "sku": {
                            "type": "text",
                            "fielddata": True,
                        },
                        "description": {
                            "type": "text",
                            "fielddata": True,
                            "analyzer": "word_join_analyzer"
                        },
                        "store": {
                            "type": "text",
                            "fielddata": True,
                            "analyzer": "word_join_analyzer"
                        }
                    }
                }

    try:
        es_object.indices.put_mapping(doc_type='price', body=mappings, index='price')
        print("mappings was updated")
    except Exception as e:
        print('Error: ', e)


def insert_docs(index_name, index=None):
    es_object = _contsants.es_object
    products = read_csv_item('price_table.csv')[index:] if index is not None else read_csv_item('price_table.csv')

    len_products = len(read_csv_item('price_table.csv'))
    remaining = index if index is not None else 1

    try:
        for i in products:
            percentage = "%.2f" % round(remaining / len_products * 100, 2)
            es_object.index(index=index_name, doc_type='price', body=i)
            print('%s was inserted (%s/%s) %s%s' % (i.get('name'), remaining, len_products, percentage, '%'))
            remaining += 1
    except Exception as e:
        print('Error in indexing document: ', str(e))
    print('%s documents was inserted..' % remaining)

def search(index_name, keyword):

    es_object = _contsants.es_object

    query = {
        'query' : {
            'multi_match' : {
                'fields' : ['name', 'catalog_code'],
                'query' : keyword,
                'fuzziness' : 'AUTO'
            }
        }
    }
    results = es_object.search(index_name, body=query)['hits']['hits']

    prod = list()
    for result in results:
        id = result['_source']['id']
        name = result['_source']['name']
        brand = result['_source']['brand']
        price_before_discount = result['_source']['price_before_discount']
        price = result['_source']['price']
        sku = result['_source']['sku']
        supplier_code = result['_source']['supplier_code']
        description = result['_source']['price']
        item_url = result['_source']['item_url']
        store = result['_source']['store']
        last_update = result['_source']['last_update']
        daruma_code = result['_source']['daruma_code']
        rating = result['_source']['rating']
        store_url = result['_source']['store_url']
        image_url = result['_source']['image_url']
        link_id = result['_source']['link_id']
        sales = result['_source']['sales']
        views = result['_source']['views']
        reviews = result['_source']['reviews']
        total_sales = result['_source']['total_sales']
        d = dict(
            id=id,
            name=name,
            brand=brand,
            price_before_discount=price_before_discount,
            price=price,
            sku=sku,
            supplier_code=supplier_code,
            description=description,
            item_url=item_url,
            store=store,
            last_update=last_update,
            daruma_code=daruma_code,
            rating=rating,
            store_url=store_url,
            image_url=image_url,
            link_id=link_id,
            sales=sales,
            views=views,
            reviews=reviews,
            total_sales=total_sales
        )
        prod.append(d)
    return prod

def delete():
    url = 'http://%s:%s/_all' % (_contsants.HOST, _contsants.PORT)
    response = requests.delete(url=url)
    result = response.json().get('acknowledged', None)
    if result:
        if result == True:
            print('index deleted..')
        else:
            print('index not deleted')
    else:
        print('Error..')

def put_aliases():
    url = 'http://%s:%s/_aliases' % (_contsants.HOST, _contsants.PORT)
    data = {
                "actions": [
                    { "add": {
                        "alias": "price",
                        "index": "price_v1"
                    }}
                ]
            }
    response = requests.post(url=url, data=json.dumps(data, indent=4))
    result = response.json()
    print(result)

def restart(index_name):
    delete()
    create_index(index_name)

def suggest_completion(index_name, prefix=None):
    if not prefix:
        return []

    es_object = _contsants.es_object

    suggest = {
        'keyword_suggest': {
            'prefix': prefix,
            'completion': {
                'field': 'suggest',
                'size': 20,
                'skip_duplicates': True,
                'fuzzy': {
                    'fuzziness': 'AUTO'
                }
            }
        }
    }

    matches = es_object.search(
        index=index_name,
        doc_type='products',
        body={ 'suggest' : suggest }
    )

    print(json.dumps(matches, indent=4))

def read_csv_item(csvfile):
    result = []
    with open(csvfile, newline='', encoding='utf-8') as f:
        readers = csv.reader(f)
        for row in readers:
            id = row[0]
            name = row[1]
            brand = row[2]
            price_before_discount = row[3]
            price = row[4]
            sku = row[5]
            supplier_code = row[6]
            description = row[7]
            item_url = row[8]
            store = row[9]
            last_update = row[10]
            daruma_code = row[11]
            rating = row[12]
            store_url = row[13]
            image_url = row[14]
            link_id = row[15]
            sales = row[16]
            views = row[17]
            reviews = row[18]
            total_sales = row[19]
            result.append(dict(
                id=id,
                name=name,
                brand=brand,
                price_before_discount=price_before_discount,
                price=price,
                sku=sku,
                supplier_code=supplier_code,
                description=description,
                item_url=item_url,
                store=store,
                last_update=last_update,
                daruma_code=daruma_code,
                rating=rating,
                store_url=store_url,
                image_url=image_url,
                link_id=link_id,
                sales=sales,
                views=views,
                reviews=reviews,
                total_sales=total_sales
            ))
    return result[1:]

if __name__ == '__main__':
    # restart('price')
    # print(len(_contsants.products))
    # print(json.dumps(search(index_name='daruma', keyword='ThreadlockerAnaerobic'), indent=4))
    # suggest_completion(index_name='daruma', prefix='loc')
    # delete()
    # create_index("price")
    # print(search('price', 'loctite'))
    insert_docs('price')
    # search('price', 'showa')
    # print(read_csv_item('price_table.csv')[0])
    # print(read_csv_item('price_table.csv')[1])
    # put_mappings()
    # create_index('price')
    # '6460319518'